# Provides UDP syslog reception  
module(load="imudp")
input(type="imudp" address="127.0.0.1" port="515")

$LocalHostName {{ f_LocalHostName }}
$PreserveFQDN on

main_queue(
  queue.workerThreads="2"
  queue.dequeueBatchSize="1000"
  queue.highWatermark="30000"    # max no. of events to hold in memory
  queue.lowWatermark="5000"     # use memory queue again, when it's back to this level
  queue.spoolDirectory="/var/spool/rsyslog/queues"  # where to write on disk
  queue.fileName="stats_ruleset"
  queue.maxDiskSpace="1g"        # it will stop at this much disk space
  queue.size="10000"           # or this many messages
  queue.saveOnShutdown="on"      # save memory queue contents to disk when rsyslog is exiting
)

template(name="ls_json" type="list" option.json="on") {
  constant(value="{")
  constant(value="\"@timestamp\":\"") property(name="timereported" dateFormat="rfc3339")
  constant(value="\",\"@version\":\"3")
  constant(value="\",\"message\":\"") property(name="msg")
  constant(value="\",\"host\":\"") property(name="fromhost")
  constant(value="\",\"fromhost\":\"") property(name="$!hostname")
  constant(value="\",\"severity\":\"") property(name="syslogseverity-text")
  constant(value="\",\"facility\":\"") property(name="syslogfacility-text")
  constant(value="\",\"programname\":\"") property(name="programname")
  constant(value="\",\"procid\":\"") property(name="procid")
  constant(value="\",\"syslogtag\":\"") property(name="SysLogTag")
  constant(value="\",\"logtype\":\"") property(name="$!logtype")
  constant(value="\"}\n")
}
set $!logtype = 'LogRsyslogJson';
set $!hostname = '{{ f_LocalHostName }}';

###LOG kamailio
template(name="t_jsonf" type="list" option.json="on") {
  constant(value="{")
  constant(value="\"@timestamp\":\"") property(name="timereported" dateFormat="rfc3339")
  constant(value="\",\"@version\":\"3")
  constant(value="\",\"message\":\"") property(name="msg")
  constant(value="\",\"severity\":\"") property(name="syslogseverity-text")
  constant(value="\",\"facility\":\"") property(name="syslogfacility-text")
  constant(value="\",\"programname\":\"") property(name="programname")
  constant(value="\",\"procid\":\"") property(name="procid")
  constant(value="\",\"syslogtag\":\"") property(name="SysLogTag")
  constant(value="\",\"logtype\":\"") property(name="$!logtype")
  constant(value="\"}\n")
}
if ( $syslogtag startswith "docker-kamailio") then {action(type="omfile" template="t_jsonf" file="/var/log/dockers/kamailio-stdout.log") stop }
if ( $syslogtag startswith "docker-freeswitch") then {action(type="omfile" template="t_jsonf" file="/var/log/dockers/freeswitch-stdout.log") stop }
if ( $syslogtag startswith "uwsgi") then {action(type="omfile" template="t_jsonf" file="/var/log/uwsgi-stdout.log") stop }

if ( $!logtype contains 'LogRsyslogJson' ) then {action(type="omfwd"  Target="{{ f_server_logs_host }}"  Port="{{ f_server_logs_port }}" Protocol="tcp" template="ls_json" queue.type="LinkedList" queue.size="10000" queue.filename="q_sendToLogserver_rsyslog_json" queue.highwatermark="9000" queue.lowwatermark="50" queue.maxdiskspace="500m" queue.saveonshutdown="on" action.resumeRetryCount="-1" action.reportSuspension="on" action.reportSuspensionContinuation="on" action.resumeInterval="10") } 

if ( $!logtype contains 'LogRsyslogJson' ) then {action(type="omfile" template="t_jsonf" file="/var/log/full-stdout.log") }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $programname startswith 'dispatcher-convy' ) then {action(type="omfile" template="t_jsonf" file="/var/log/dockers/dispatcher-convy.log") stop }

if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'auth' ) then  {action(type="omfile" template="t_jsonf" file="/var/log/auth.log") stop }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'daemon' ) then {action(type="omfile" template="t_jsonf" file="/var/log/daemon.log") stop }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'cron' ) then  {action(type="omfile" template="t_jsonf" file="/var/log/cron.log") stop }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'lpr' ) then  {action(type="omfile" template="t_jsonf" file="/var/log/lpr.log") stop }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'mail' ) then  {action(type="omfile" template="t_jsonf" file="/var/log/mail.log") stop }
if ( $!logtype contains 'LogRsyslogJson' )  and ( $syslogfacility-text startswith 'debug' ) then  {action(type="omfile" template="t_jsonf" file="/var/log/debug.log") stop }




